"""
Implements a Island with many Landscapes.
"""

import numpy as np

from biosim.landscapes import Highland, Lowland, Desert, Water

__author__ = 'Aslak Djupskaas & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskas@nmbu.no or dinussen.sivarasalingam@nmbu.no'


class Island:
    """
    Makes a island based on a given map
    """

    def __init__(self, geogr):
        """
        Creates an island

        Parameters
        ----------
        geogr : string
            Map of the island

        """
        self.map = geogr
        self.geogr = self.set_map()
        self.check_boundary()

    @property
    def all_herbivores(self):
        """
         Makes a nested list of all the Herbivores

         Returns
         -------
         list
            list of all Herbivores at the island
         """

        all_herbs = []
        all_herbs_nested = [landscape.herbivores_pop for landscape in self.geogr.values()
                            if landscape.get_num_herb > 0]
        for row in all_herbs_nested:
            all_herbs.extend(row)
        return all_herbs

    @property
    def all_carnivores(self):
        """
        Makes a nested list of all the Carnivores

        Returns
        -------
        list
            list of all carnivores at the island
        """
        all_carn = []
        all_carn_nested = [landscape.carnivores_pop for landscape in self.geogr.values()
                           if landscape.get_num_carn > 0]
        for row in all_carn_nested:
            all_carn.extend(row)
        return all_carn

    def set_map(self):
        """
        Makes the map og the island to a dictionary with location as key
         and object of landscape as value

        Returns
        -------
        dict
            Returns the map in a dict
        """
        valid = ['W', 'L', 'H', 'D']
        landscapes = {}
        for letter in self.map:
            if letter != '\n':
                if letter in landscapes:
                    landscapes[letter] += 1
                else:
                    landscapes[letter] = 1
                if letter not in valid:
                    raise ValueError('Enter valid landscape')

        cells = {}
        for letter, value in landscapes.items():
            if letter == 'L':
                cells['L'] = [Lowland() for _ in range(value)]
            elif letter == 'H':
                cells['H'] = [Highland() for _ in range(value)]
            elif letter == 'W':
                cells['W'] = [Water() for _ in range(value)]
            elif letter == 'D':
                cells['D'] = [Desert() for _ in range(value)]
        geogr = {}
        for row, line in enumerate(self.map.split('\n')):
            for col, letter in enumerate(line):
                geogr[(row + 1, col + 1)] = cells[letter][0]
                cells[letter].remove(cells[letter][0])
        return geogr

    def check_boundary(self):
        """
        Raises ValueError if there are difference in length of each row in the map
        Raises ValueError if the edge of the island is not water
        """
        rows = [len(line) for line in self.map.split('\n')]
        if len(set(rows)) != 1:
            raise ValueError('Inconsistent length of map')
        for pos, land in self.geogr.items():
            if pos[0] == 1 or pos[1] == 1:
                if land.type != 'water':
                    raise ValueError('Invalid map. Must be water at the boundary')

    @property
    def count_all_herbivores(self):
        """
        Sums up all Herbivores

        Returns
        -------
        int
            number of Herbivores
        """
        return sum([landscapes.get_num_herb for landscapes in self.geogr.values()])

    @property
    def count_all_carnivores(self):
        """
        Sums up all Carnivores

        Returns
        -------
        int
            number of Carnivores
        """
        return sum([landscapes.get_num_carn for landscapes in self.geogr.values()])

    @property
    def count_all_animals(self):
        """
        Sums up all Animals

        Returns
        -------
        int
            number of Animals at island
        """
        return self.count_all_herbivores + self.count_all_carnivores

    def place_animal(self, pos, num_herb=0, herb_weight=20,
                     herb_age=5, num_carn=0, carn_weight=20, carn_age=5):
        """
        Places the animal

        Parameters
        ----------
        pos : tuple
            position of the placement
        num_herb : int
            number of Herbivores
        num_carn : int
            number of Carnivores
        herb_weight : float
            weight of Herbivores
        carn_weight : float
            weight of Carnivores
        herb_age : int
            age of the Herbivores that gets placed
        carn_age : int
            age of the Carnivores that gets placed
        """
        if self.geogr[pos].type == 'water':
            raise ValueError('Not possible to place animals in water')
        else:
            self.geogr[pos].extendpop(num_herb, num_carn,
                                      pos, herb_weight, herb_age, carn_weight, carn_age)

    @property
    def density_matrix_herb(self):
        """
        First: Creates a numpy matrix with the same size as the map.
        Takes the geogr- dictionary and counts Herbivores in each landscape
        and place them at the corresponding cell in the matrix.

        Returns
        -------
        matrix
            with numbers of herbivores in each position

        """
        row = len(self.map.split('\n'))
        col = len(self.map.split('\n')[0])
        matrix = np.zeros(shape=(row, col))

        for i, line in enumerate(matrix):
            for j, letter in enumerate(line):
                for pos, landscape in self.geogr.items():
                    if pos == (i + 1, j + 1):
                        matrix[i, j] = landscape.get_num_herb
        return matrix

    @property
    def density_matrix_carn(self):
        """
        First: Creates a numpy matrix with the same size as the map.
        Takes the geogr- dictionary and counts Carnivores in each landscape and place
        them at the corresponding cell in the matrix.

        Returns
        -------
        matrix
            with numbers of carnivores in each position
        """
        row = len(self.map.split('\n'))
        col = len(self.map.split('\n')[0])
        matrix = np.zeros(shape=(row, col))

        for i, line in enumerate(matrix):
            for j, letter in enumerate(line):
                for pos, landscape in self.geogr.items():
                    if pos == (i + 1, j + 1):
                        matrix[i, j] = landscape.get_num_carn
        return matrix

    def run_cycle(self):
        """

       * Runs the yearly circle on the island:
            * All animals eat
            * Checks all animals if they give birth, then add the new born to the population
            * Checks all animals if they migrate, them migrate them
            * Age all animals one year
            * All animals lose some weight
            * Checks all animals if they die and remove the dead from the population.
        """
        # Feeding
        [landscapes.feed() for landscapes in self.geogr.values()]

        # Procreation
        [landscapes.procreation() for landscapes in self.geogr.values()]

        # Migration
        all_herbivores = []
        all_carnivores = []
        for pos, landscape in self.geogr.items():
            if len(landscape.herbivores_pop) > 0 or len(landscape.carnivores_pop) > 0:
                herbivores, carnivores = landscape.choose_cell(pos, self.geogr)
                all_herbivores.extend(herbivores)
                all_carnivores.extend(carnivores)
        [landscape.add_migrated_animals(all_herbs=all_herbivores, all_carns=all_carnivores, pos=pos)
         for pos, landscape in self.geogr.items()]

        # Aging
        [landscapes.aging() for landscapes in self.geogr.values()]

        # Weight lost
        [landscapes.weight_lost() for landscapes in self.geogr.values()]

        # Deaths
        [landscapes.deaths() for landscapes in self.geogr.values()]
