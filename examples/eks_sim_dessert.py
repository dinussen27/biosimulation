# -*- coding: utf-8 -*-

import textwrap
from biosim.simulation import BioSim

"""
checks if all animal dies in dessert
"""

__author__ = 'Aslak Djupskås & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskås@nmbu.no or dinussen.sivarasalingam@nmbu.no'

if __name__ == '__main__':

    geogr = """\
           WWW
           WDW
           WWW"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(90)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                 seed=99004,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 50.0, 'delta': 5},
                             'weight': {'max': 100, 'delta': 5}},
                 vis_years=2, img_years=4,ymax_animals=100)

    sim.simulate(num_years=40)

