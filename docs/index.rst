.. Aslak & Dinussen documentation master file, created by
   sphinx-quickstart on Mon Jan 17 10:49:13 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioSim documentation!
============================================

This is a project task given by EPAP to simulate the dynamics of the animals on Rossumoya
   This is a simple simulation of
      * An island
      * with four different landscapes
      * containing two species


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   animal_landscape_island
   biosim_and_graphics


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
