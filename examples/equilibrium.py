"""
Island with single lowland cell, first herbivores only, later carnivores.
"""


__author__ = 'Hans Ekkehard Plesser, NMBU'


import textwrap
from biosim.simulation import BioSim
from biosim.animals import Carnivores

geogr = """\
           WWWW
           WLLW
           WWWW"""
geogr = textwrap.dedent(geogr)

ini_herbs = [{'loc': (2, 2),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(150)]}]
ini_carns = [{'loc': (2, 2),
              'pop': [{'species': 'Carnivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(25)]}]


Carnivores.set_params({'DeltaPhiMax': 15})
sim = BioSim(geogr, ini_herbs, seed=1234,
             img_dir='results', img_base=f'mono_hc_{1234:05d}', img_years=75, ymax_animals=600 )
sim.simulate(50)
sim.add_population(ini_carns)
sim.simulate(100)
