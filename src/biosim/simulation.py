"""
The BioSim simulation simulate a island with a predator specie and a prey specie.


Example
--------
::

    geogr =
        WWWWWWWWWWWWWWWWWWWWWWW
        WWWWWWWWHWWWWLLLLLLLWWW
        WHHHHHLLLLWWLLLLLLLDWWW
        WHHHHHHHHHWWLLLLLLDDWWW
        WHHHHHLLLLWLLLLLLLWDWWW
        WHHHHHLLLDWLLLHLLLWDWWW
        WHHLLLLLDDDLLLHHHHLDWWW
        WWHHHHLLLDDLLLHWWLLLWWW
        WHHHLLLLLDWLLLLLLLWWWWW
        WHHHHLLLLDWLLLLLLHHWWWW
        WWHHHHLLLLLLLLLWWHHWWWW
        WWWHHHHLLLLLLLLHHHHWWWW
        WWHHHHHHLLWLLLLLLWWWWWW
        WWWWWWWWWWWWWWWWWWWWWWW

    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (11, 11), 'pop': [{'species':
     'Herbivore', 'age': 5, 'weight': 20} for _ in range(90)]}]
    ini_carns = [{'loc': (10, 9), 'pop': [{'species':
     'Carnivore', 'age': 5, 'weight': 20} for _ in range(40)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs,seed=99004,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 50.0, 'delta': 5},
                 'weight': {'max': 100, 'delta': 5}},
                 vis_years=1)

        sim.set_animal_parameters('Herbivore', {'zeta': 3.2, 'xi': 1.8})
        sim.set_animal_parameters('Carnivore', {'a_half': 70, 'phi_age': 0.5,
                                    'omega': 0.3, 'F': 65, 'DeltaPhiMax': 9.})
        sim.set_landscape_parameters('L', {'f_max': 700})

    sim.simulate(num_years=40)
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=160)


.. image:: ../src/biosim/sim_200.png
    :class: with-border

Picture of the end result for the given parameters above.

.. note::

    This code creates a system with island by the given specs, puts a initial
    population on the island, changes the herbivoreand carnivore default
    specs and sets a upper limit for the graph that shows the animals.
    and it performs a simulation of 40 years with Herbivore alone, then adds Carnivores
    and runs the simulation 160 more years updating the graphics after each given step and
    saving a figure after each step.


.. note::

    The BioSim simulation was inspired from the BioLab simulation,
    Chutes project, plotting, Randvis project
    and our lecture notes which were all made by our Professor Hans Ekkel Plesser.
"""
import random

from .graphics import Graphics
from .island import Island
from .landscapes import Lowland, Highland, Desert
from .animals import Herbivores, Carnivores
import matplotlib.pyplot as plt

__author__ = 'Aslak Djupskås & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskås@nmbu.no or dinussen.sivarasalingam@nmbu.no'

# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2021 Hans Ekkehard Plesser / NMBU


class BioSim:
    def __init__(self, island_map, ini_pop, seed=1234567,
                 vis_years=1, ymax_animals=12500, cmax_animals=None, hist_specs=None,
                 img_dir="results", img_base="sim", img_fmt='png', img_years=1.,
                 log_file=None):

        """
        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param hist_specs: Specifications for histograms, see below
        :param vis_years: years between visualization updates (if 0, disable graphics)
        :param img_dir: String with path to directory for figures
        :param img_base: String with beginning of file name for figures
        :param img_fmt: String with file type for figures, e.g. 'png'
        :param img_years: years between visualizations saved to files (default: vis_years)
        :param log_file: If given, write animal counts to this file

        If ymax_animals is None, the y-axis limit should be adjusted automatically.
        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
           {'Herbivore': 50, 'Carnivore': 20}

        hist_specs is a dictionary with one entry per property for which a histogram shall be shown.
        For each property, a dictionary providing the maximum value and the bin width must be
        given, e.g.,
            {'weight': {'max': 80, 'delta': 2}, 'fitness': {'max': 1.0, 'delta': 0.05}}
        Permitted properties are 'weight', 'age', 'fitness'.

        If img_dir is None, no figures are written to file. Filenames are formed as

            f'{os.path.join(img_dir, img_base}_{img_number:05d}.{img_fmt}'

        where img_number are consecutive image numbers starting from 0.

        img_dir and img_base must either be both None or both strings.
        """
        self.island_map = island_map
        self.ini_pop = ini_pop
        self.seed = seed
        self.vis_years = vis_years
        self.ymax_animals = ymax_animals
        self.cmax_animals = cmax_animals
        self.hist_specs = hist_specs
        self.img_years = img_years
        self.img_dir = img_dir
        self.log_file = log_file
        self.island = Island(island_map)
        self.add_population(ini_pop)
        self.vis = Graphics(img_dir, img_base, img_fmt)
        self.current_year = 0

    def set_animal_parameters(self, species, params):
        """
        Set parameters for animal species.

        :param species: String, name of animal species
        :param params: Dict with valid parameter specification for species
        """
        if species == 'Herbivore':
            Herbivores.set_params(params)
        elif species == 'Carnivore':
            Carnivores.set_params(params)
        else:
            raise ValueError('Animal not on Island')

    def set_landscape_parameters(self, landscape, params):
        """
        Set parameters for landscape type.

        :param landscape: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """
        if landscape == 'L':
            Lowland.set_params(params)
        elif landscape == 'H':
            Highland.set_params(params)
        elif landscape == 'D':
            Desert.set_params(params)
        else:
            raise ValueError('Enter a valid landscape at island')

    def simulate(self, num_years):
        """
        Run simulation while visualizing the result.

        :param num_years: number of years to simulate
        """

        if self.log_file is not None:
            outfile = open(self.log_file, 'w')

        if self.vis_years != 0:
            self.vis.setup((self.current_year+num_years),
                           self.vis_years, self.ymax_animals,
                           self.island_map, self.vis_years)
            if num_years % self.vis_years != 0:
                raise ValueError('totalyear is not divisible with vis updates')
            runningtime = num_years/self.vis_years
        else:
            runningtime = num_years

        random.seed(self.seed)

        for year in range(int(runningtime)):
            if self.vis_years != 0:
                for year2 in range(self.vis_years):
                    self.island.run_cycle()
            else:
                self.island.run_cycle()

            if self.log_file is not None:
                outfile.write(str(self.num_animals_per_species))
                outfile.write('\n')
            if self.vis_years != 0:
                 self.vis.update(self.year, self.island, self.cmax_animals,
                                 self.hist_specs, self.current_year)

                 if self.current_year % self.img_years == 0:
                     self.vis._save_graphics(self.current_year, self.img_years)
                 plt.pause(0.001)
            if self.vis_years != 0:
                self.current_year += self.vis_years
            else:
                self.current_year += 1
        self.make_movie()

    def add_population(self, population):
        """
        Add a population to the island

        :param population: List of dictionaries specifying population
        """
        for placement in population:
            location = placement['loc']
            for spawn in placement['pop']:
                age = spawn['age']
                weight = spawn['weight']
                if spawn['species'] == 'Herbivore':
                    self.island.place_animal(pos=location, num_herb=1,
                                             herb_weight=weight, herb_age=age)
                elif spawn['species'] == 'Carnivore':
                    self.island.place_animal(pos=location, num_carn=1,
                                             carn_weight=weight, carn_age=age)
                else:
                    raise ValueError('This animal does not exist at Rossumøya')

    @property
    def year(self):
        """Last year simulated."""
        return self.current_year

    @property
    def num_animals(self):
        """Total number of animals on island."""
        return self.island.count_all_carnivores + self.island.count_all_herbivores

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""
        return {'Herbivore': self.island.count_all_herbivores,
                'Carnivore': self.island.count_all_carnivores}

    def make_movie(self, movie_format="mp4"):
        """Create MPEG4 movie from visualization images saved."""
        if self.vis_years != 0:
            self.vis.make_movie(movie_format)
        else:
            return
