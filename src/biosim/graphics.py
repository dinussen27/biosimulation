"""
:mod:`biosim.graphics` provides graphics for det BioSim simulation.

.. note::
   * This module requires the program ``ffmpeg`` or ``convert``
     available from `<https://ffmpeg.org>` and `<https://imagemagick.org>`.
   * You can also install ``ffmpeg`` using ``conda install ffmpeg``
   * You need to set the  :const:`_FFMPEG_BINARY` and :const:`_CONVERT_BINARY`
     constants below to the command required to invoke the programs
   * You need to set the :const:`_DEFAULT_FILEBASE` constant below to the
     directory and file-name start you want to use for the graphics output
     files.

"""

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import os
import textwrap

__author__ = 'Aslak Djupskås & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskås@nmbu.no or dinussen.sivarasalingam@nmbu.no'

# Update these variables to point to your ffmpeg and convert binaries
# If you installed ffmpeg using conda or installed both softwares in
# standard ways on your computer, no changes should be required.
_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_GRAPHICS_DIR = os.path.join('../..', 'data')
_DEFAULT_GRAPHICS_NAME = 'dv'
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'   # alternatives: mp4, gif


class Graphics:
    """Provides graphics support for BioSim."""

    def __init__(self, img_dir=None, img_name=None, img_fmt=None):
        """
        :param img_dir: directory for image files; no images if None
        :type img_dir: str
        :param img_name: beginning of name for image files
        :type img_name: str
        :param img_fmt: image file format suffix
        :type img_fmt: str
        """

        if img_name is None:
            img_name = _DEFAULT_GRAPHICS_NAME

        if img_dir is not None:
            if not os.path.isdir(img_dir):
                os.mkdir(img_dir)
            self._img_base = os.path.join(img_dir, img_name)
        else:
            self._img_base = _DEFAULT_GRAPHICS_DIR

        self._img_fmt = img_fmt if img_fmt is not None else _DEFAULT_IMG_FORMAT

        self._img_ctr = 0

        # the following will be initialized by _setup_graphics
        self._fig = None
        self._map_ax = None
        self._img_island_ax = None
        self._img_herb_axis = None
        self._img_carn_axis = None
        self._img_fit_axis = None
        self._img_age_axis = None
        self._img_weight_axis = None
        self._count_ax = None
        self._herb_line = None
        self._herb_dens_ax = None
        self._carn_line = None
        self._carn_dens_ax = None
        self._fit_ax = None
        self._age_ax = None
        self._weight_ax = None
        self._year_ax = None
        self._txt = None
        self.template = None
        self._img_island_ax_lg = None
        self._img_axis = None
        self._img_step = None
        self._gridspec = None

    def make_movie(self, movie_fmt=None):
        """
        Creates MPEG4 movie from visualization images saved.

        .. :note:
            Requires ffmpeg for MP4 and magick for GIF

        The movie is stored as img_base + movie_fmt
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt is None:
            movie_fmt = _DEFAULT_MOVIE_FORMAT

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self._img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self._img_base),
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def setup(self, final_step, img_step, ymax_animals, island_map, vis_years):
        """
        Prepare graphics.

        Call this before running the simulation`to set up the plots.

        :param final_step: last time step to be visualised (upper limit of x-axis)
        :param img_step: interval between saving image to file
        :param max_animal: max y-value for animal count plot
        :param island_map: uses map of island and plots an image of it
        """

        self._img_step = img_step

        # create new figure window
        if self._fig is None:
            self._fig = plt.figure(figsize=(12, 8))
            self._gridspec = self._fig.add_gridspec(9, 18)

        # Add left subplot for images created with imshow().
        # We cannot create the actual ImageAxis object before we know
        # the size of the image, so we delay its creation.
        if self._img_island_ax is None:
            self._img_island_ax = self._fig.add_subplot(self._gridspec[:3, :4])
            plt.title('Island')
            self._img_axis = None
            island_map = textwrap.dedent(island_map)

            #                   R    G    B
            rgb_value = {'W': (0.0, 0.0, 1.0),  # blue
                         'L': (0.0, 0.6, 0.0),  # dark green
                         'H': (0.5, 1.0, 0.5),  # light green
                         'D': (1.0, 1.0, 0.5)}  # light yellow

            map_rgb = [[rgb_value[column] for column in row]
                       for row in island_map.splitlines()]
            self._img_island_ax.imshow(map_rgb)

            self._img_island_ax_lg = self._fig.add_subplot(self._gridspec[:2, 4:5])
            self._img_island_ax_lg.axis('off')
            for ix, name in enumerate(('Water', 'Lowland',
                                       'Highland', 'Desert')):
                self._img_island_ax_lg.add_patch(plt.Rectangle((0.91, ix * 0.17), 0.125, 0.125,
                                                 edgecolor='none',
                                                 facecolor=rgb_value[name[0]]))
                self._img_island_ax_lg.text(1.01, ix * 0.17, name,
                                            transform=self._img_island_ax_lg.transAxes)

        if self._count_ax is None:
            self._count_ax = self._fig.add_subplot(self._gridspec[:3, 12:18])
            plt.title('Animal count')
            self._count_ax.set_ylim(0, ymax_animals)

        if self._herb_dens_ax is None:
            self._herb_dens_ax = self._fig.add_subplot(self._gridspec[4:7, 2:8])
            plt.title('Herbivore Density Map')
            self._img_herb_axis = None

        if self._carn_dens_ax is None:
            self._carn_dens_ax = self._fig.add_subplot(self._gridspec[4:7, 10:16])
            plt.title('Carnivore Density Map')
            self._img_carn_axis = None

        if self._fit_ax is None:
            self._fit_ax = self._fig.add_subplot(self._gridspec[8:, 1:6])
            self._fit_ax.set_title('Fitness distribution')
            self._img_fit_axis = None

        if self._age_ax is None:
            self._age_ax = self._fig.add_subplot(self._gridspec[8:, 7:12])
            self._age_ax.set_title('Age distribution')
            self._img_age_axis = None

        if self._weight_ax is None:
            self._weight_ax = self._fig.add_subplot(self._gridspec[8:, 13:18])
            self._weight_ax.set_title('Weight distribution')
            self._img_weight_axis = None

        if self._year_ax is None:
            self._year_ax = self._fig.add_subplot(self._gridspec[:3, 6:12])  # llx, lly, w, h
            self._year_ax.axis('off')  # turn off coordinate system
            self.template = 'Year: {:5d}'
            self._txt = self._year_ax.text(0.5, 0.5, self.template.format(0),
                                           horizontalalignment='center',
                                           verticalalignment='center',
                                           transform=self._year_ax.transAxes)

        # needs updating on subsequent calls to simulate()
        # add 1 so we can show values for time zero and time final_step
        self._count_ax.set_xlim(0, final_step+1)

        if vis_years != 1:
            line = 'bo'
        else:
            line = 'b-'

        if self._herb_line is None:
            herb_plot = self._count_ax.plot(np.arange(0, final_step+1),
                                            np.full(final_step+1, np.nan), line, label='Herbivore')
            self._herb_line = herb_plot[0]
        else:
            x_data, y_data = self._herb_line.get_data()
            x_new = np.arange(x_data[-1] + 1, final_step+1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._herb_line.set_data(np.hstack((x_data, x_new)),
                                         np.hstack((y_data, y_new)))

        if vis_years != 1:
            line = 'ro'
        else:
            line = 'r-'

        if self._carn_line is None:
            carn_plot = self._count_ax.plot(np.arange(0, final_step+1),
                                            np.full(final_step+1, np.nan), line, label='Carnivores')
            self._carn_line = carn_plot[0]
        else:
            x_data, y_data = self._carn_line.get_data()
            x_new = np.arange(x_data[-1] + 1, final_step+1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._carn_line.set_data(np.hstack((x_data, x_new)),
                                         np.hstack((y_data, y_new)))

        self._count_ax.legend(handles=[self._herb_line, self._carn_line])

    def update_year(self, year):
        """Updates the year parameter"""
        self._txt.set_text(self.template.format(year))

    def update_mean_graph(self, year, total_herb, total_carn):
        """Updates the line with total for Herbivores and Carnivores."""
        y_data = self._herb_line.get_ydata()
        y_data[year] = total_herb
        self._herb_line.set_ydata(y_data)

        y_data = self._carn_line.get_ydata()

        y_data[year] = total_carn
        self._carn_line.set_ydata(y_data)

        # These lines of code makes the ylim in count animals dynamic
        # if self._count_ax.get_ylim()[1] < max(total_carn, total_herb):
        #    self._count_ax.set_ylim(0, max(total_carn, total_herb)+150)

    def update_heatmap_herb(self, sys_map_herb, cmax_animals):
        """Updates the heatmap for the Herbivores."""
        if cmax_animals is not None:
            cmax = cmax_animals['Herbivore']
        else:
            cmax = 150

        if self._img_herb_axis is not None:
            self._img_herb_axis.set_data(sys_map_herb)
        else:
            self._img_herb_axis = self._herb_dens_ax.imshow(sys_map_herb,
                                                            interpolation='nearest',
                                                            vmin=0, vmax=cmax)
            plt.colorbar(self._img_herb_axis, ax=self._herb_dens_ax,
                         orientation='vertical')

    def update_heatmap_carn(self, sys_map_carn, cmax_animals):
        """Updates the heatmap for the Carnivores."""
        if cmax_animals is not None:
            cmax = cmax_animals['Carnivore']
        else:
            cmax = 100

        if self._img_carn_axis is not None:
            self._img_carn_axis.set_data(sys_map_carn)
        else:
            self._img_carn_axis = self._carn_dens_ax.imshow(sys_map_carn,
                                                            interpolation='nearest',
                                                            vmin=0, vmax=cmax)
            plt.colorbar(self._img_carn_axis, ax=self._carn_dens_ax,
                         orientation='vertical')

    def update_fit(self, island, hist_specs):
        """Updates the histogram with fitness for Herbivores and Carnivores."""
        if hist_specs is not None:
            fit_specs = hist_specs['fitness']
            bins = int(fit_specs['max']/fit_specs['delta'])
            x_max = fit_specs['max']
        else:
            bins = 20
            x_max = 1

        fitness_herb = np.array([herb.fit for herb in island.all_herbivores])
        fitness_carn = np.array([carn.fit for carn in island.all_carnivores])
        if self._img_fit_axis is not None:
            self._fit_ax.clear()
            self._fit_ax.set_title('Fitness distribution')
            self._fit_ax.hist(fitness_herb, edgecolor='blue',
                              color='white', bins=bins, histtype='step')
            self._fit_ax.hist(fitness_carn, edgecolor='red',
                              color='white', bins=bins, histtype='step')
            self._fit_ax.set_xlim(0, x_max)

        else:
            self._img_fit_axis = self._fit_ax.hist(fitness_herb, edgecolor='blue',
                                                   color='white', bins=bins, histtype='step')[0]
            self._img_fit_axis = self._fit_ax.hist(fitness_carn, edgecolor='red',
                                                   color='white', bins=bins, histtype='step')[0]

    def update_age(self, island, hist_specs):
        """Updates the histogram with age for Herbivores and Carnivores."""
        if hist_specs is not None:
            age_specs = hist_specs['age']
            bins = int(age_specs['max']/age_specs['delta'])
            x_max = age_specs['max']
        else:
            bins = 20
            x_max = 60

        age_herb = np.array([herb.age for herb in island.all_herbivores])
        age_carn = np.array([carn.age for carn in island.all_carnivores])
        if self._img_age_axis is not None:
            self._age_ax.clear()
            self._age_ax.set_title('Age distribution')
            self._age_ax.hist(age_herb, edgecolor='blue', color='white', bins=bins, histtype='step')
            self._age_ax.hist(age_carn, edgecolor='red', color='white', bins=bins, histtype='step')
            self._age_ax.set_xlim(0, x_max)

        else:
            self._img_age_axis = self._age_ax.hist(age_herb, edgecolor='blue', color='white',
                                                   bins=bins, histtype='step')[0]
            self._img_age_axis = self._age_ax.hist(age_carn, edgecolor='red', color='white',
                                                   bins=bins, histtype='step')[0]

    def update_weight(self, island, hist_specs):
        """Updates the histogram with weight for Herbivores and Carnivores."""
        if hist_specs is not None:
            weight_specs = hist_specs['weight']
            bins = int(weight_specs['max'] / weight_specs['delta'])
            x_max = weight_specs['max']
        else:
            bins = 20
            x_max = 60

        weight_herb = np.array([herb.weight for herb in island.all_herbivores])
        weight_carn = np.array([carn.weight for carn in island.all_carnivores])
        if self._img_weight_axis is not None:
            self._weight_ax.clear()
            self._weight_ax.set_title('Weight distribution')
            self._weight_ax.hist(weight_herb, edgecolor='blue',
                                 color='white', bins=bins, histtype='step')
            self._weight_ax.hist(weight_carn, edgecolor='red',
                                 color='white', bins=bins, histtype='step')
            self._weight_ax.set_xlim(0, x_max)

        else:
            self._img_weight_axis = self._age_ax.hist(weight_herb, edgecolor='blue', color='white',
                                                      bins=bins, histtype='step')[0]
            self._img_weight_axis = self._age_ax.hist(weight_carn, edgecolor='red', color='white',
                                                      bins=bins, histtype='step')[0]

    def update(self, year, island, cmax_animals, hist_specs, current_year):
        self.update_year(year)
        self.update_heatmap_herb(island.density_matrix_herb, cmax_animals)
        self.update_heatmap_carn(island.density_matrix_carn, cmax_animals)
        self.update_fit(island, hist_specs)
        self.update_age(island, hist_specs)
        self.update_weight(island, hist_specs)
        self.update_mean_graph(current_year, len(island.all_herbivores),
                               len(island.all_carnivores))

    def _save_graphics(self, step, imgy):
        """Saves graphics to file if:
          * file name given
          * vis_years != 0
          * step % vis_years == 0

         """

        if imgy != 0 or self._img_base is not None or (step % imgy) == 0:
            plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                         num=self._img_ctr,
                                                         type=self._img_fmt))
            self._img_ctr += 1
        else:
            return
