import pytest
import textwrap
from biosim.island import Island
from biosim.animals import Herbivores, Carnivores


@pytest.mark.parametrize('num_herbs, num_carns, pos', [(20, 10, (3, 4)), (100, 50, (6, 7))])
class TestIsland:
    """
    Parameters that is used in the class
    Different number of animals, placed in two different positions
    """
    @pytest.fixture(autouse=True)
    def setup_island(self, num_herbs, num_carns, pos):
        """
        Setup for each function
        """
        self.geogr = """\
                           WWWWWWWWWWWWWWWWWW
                           WHHHHHHWWLLLLLLWWW
                           WHHLLLLLLLLLLLLWWW
                           WHHLLLDDLLLHLLLWWW
                           WLLLLDDDLLLHHHHWWW
                           WWHLLLDDLLLHWWWWWW
                           WWWWWWWWWWWWWWWWWW"""
        self.geogr = textwrap.dedent(self.geogr)
        self.num_herbs = num_herbs
        self.num_carns = num_carns
        self.pos = pos
        self.island = Island(self.geogr)
        self.island.place_animal(pos=pos, num_herb=num_herbs, num_carn=num_carns)

    @pytest.fixture
    def reset_params(self):
        """If changing parameters, this function will reset them afterwords"""
        yield
        Carnivores.set_params(Carnivores.default_params)
        Herbivores.set_params(Herbivores.default_params)

    def test_counting_animals(self):
        """Testing counting functions in Island"""
        tot_animal = self.island.count_all_animals
        tot_herb = self.island.count_all_herbivores
        tot_carn = self.island.count_all_carnivores
        assert tot_animal == self.num_herbs + self.num_carns \
               and tot_herb == self.num_herbs and tot_carn == self.num_carns

    def test_animal_lists(self):
        """Testing that the function return a list with len(list) unique animal objects"""
        herbivores = self.island.all_herbivores
        carnivores = self.island.all_carnivores
        assert len(set(herbivores)) == len(herbivores) and len(set(carnivores)) == len(carnivores)

    def test_migration(self):
        """Test that no animal migrate to other positions than the four neighbor- positions"""
        self.island.run_cycle()
        herbivores = []
        herbivores.extend(self.island.geogr[self.pos].herbivores_pop)
        herbivores.extend(self.island.geogr[(self.pos[0] - 1, self.pos[1])].herbivores_pop)
        herbivores.extend(self.island.geogr[(self.pos[0] + 1, self.pos[1])].herbivores_pop)
        herbivores.extend(self.island.geogr[(self.pos[0], self.pos[1] + 1)].herbivores_pop)
        herbivores.extend(self.island.geogr[(self.pos[0], self.pos[1] - 1)].herbivores_pop)
        assert len(herbivores) == self.island.count_all_herbivores

    def test_no_swimmers(self):
        """test that no animals are in water after 40 years"""
        [self.island.run_cycle() for _ in range(40)]
        for landscape in self.island.geogr.values():
            if landscape.type == 'water':
                assert landscape.get_num_animals == 0

    def test_run_cycle_no_migration(self, reset_params):
        """test that no animals are migrating if 'mu' = 0"""
        Herbivores.set_params({'mu': 0})
        Carnivores.set_params({'mu': 0})
        self.island.run_cycle()
        assert self.island.geogr[self.pos].get_num_animals == self.island.count_all_animals
