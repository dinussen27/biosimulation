"""
Python package for simulatuon of Rossumoya
"""

__name__ = 'Aslak Djupskas and Dinussen Sivarasalingam'
__email__ = 'aslak.djupskas@nmbu.no, dinussan.sivarasalingam@nmbu.no'
__version__ = "0.0.1"
