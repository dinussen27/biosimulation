"""
Implements an Animal model, if nothing is specified, a default Herbivore or Carnivore is made/born

Example - Herbivore
-------------------
::

    herb = Herbivores()
    herb.older()

This code

    * creates a Herbivore with default parameters
    * performs an aging functions once and makes the animal one year older;



"""
from math import exp
import random

__author__ = 'Aslak Djupskås & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskås@nmbu.no or dinussen.sivarasalingam@nmbu.no'


class Animals:
    """
    makes an animal with given parameters, or takes standard parameters from the code
    """
    w_birth = None
    sigma_birth = None
    beta = None
    eta = None
    a_half = None
    phi_age = None
    w_half = None
    phi_weight = None
    mu = None
    gamma = None
    zeta = None
    xi = None
    omega = None
    F = None
    DeltaPhiMax = None
    default_params = {'w_birth': w_birth,
                      'sigma_birth': sigma_birth,
                      'beta': beta,
                      'eta': eta,
                      'a_half': a_half,
                      'phi_age': phi_age,
                      'w_half': w_half,
                      'phi_weight': phi_weight,
                      'mu': mu,
                      'gamma': gamma,
                      'zeta': zeta,
                      'xi': xi,
                      'omega': omega,
                      'F': F,
                      'DeltaPhiMax': DeltaPhiMax}

    @classmethod
    def set_params(cls, new_params):
        """
        set Class parameters

        Parameters
        ----------
        new_params: dict
            given parameters by user

        Raises
        -------
        KeyError, ValueError
        """

        params = ('w_birth', 'sigma_birth', 'beta', 'eta', 'a_half',
                  'phi_age', 'w_half', 'phi_weight', 'mu', 'gamma',
                  'zeta', 'xi', 'omega', 'F', 'DeltaPhiMax')

        for key, value in new_params.items():
            if key not in params:
                raise KeyError('Invalid parameter name: ' + key)
            else:
                for param_name in params:
                    if key == 'eta':
                        if not 0 <= value <= 1:
                            raise ValueError(f'{param_name} must be between 0 and 1.')
                    elif key == 'DeltaPhiMax':
                        if not 0 < value:
                            raise ValueError(f'{param_name} must be strictly positive.')
                    elif key == param_name:
                        if not 0 <= value:
                            raise ValueError(f'{param_name} must be positive.')
                    setattr(cls, key, value)

    @classmethod
    def get_params(cls):
        """
        Get class parameters.

        Returns
        -------
        dict
            Dictionary with class parameters.
        """
        return {'w_birth': cls.w_birth,
                'sigma_birth': cls.sigma_birth,
                'eta': cls.eta,
                'a_half': cls.a_half,
                'phi_age': cls.phi_age,
                'w_half': cls.w_half,
                'phi_weight': cls.phi_weight,
                'mu': cls.mu,
                'gamma': cls.gamma,
                'zeta': cls.zeta,
                'xi': cls.xi,
                'omega': cls.omega,
                'F': cls.F,
                'DeltaPhiMax': cls.DeltaPhiMax}

    def __init__(self, weight=None, age=0, pos=()):
        """
        creates a Herbivore

        Parameters
        ----------
        weight : float > 0
            weight at start
        age : int
            age at start
        pos : tuple
            position at start
        """
        self.pos = pos
        if int(age) == age:
            self.age = int(age)
        else:
            raise ValueError('Age must be integer!')

        if weight is None:
            self.weight = random.gauss(self.w_birth, self.sigma_birth)
        elif weight < 0:
            raise ValueError('Weight must be positive')
        else:
            self.weight = weight

        self.fit = self.fitness()

    def fitness(self):
        r"""
        Decides the fitness of animal

        The fitness of the animal is a result based on the age and weight of the animal.
        If:

        .. math::
            \begin{equation}
            \Phi =
            \begin{cases}
                0, & \text{w} \leq 0 \\
                q^{+} \times (a,a_{age},\Phi_{age})
                \times q^{-} \times (a,a_{age},\Phi_{age}), & \text{else}
            \end{cases}
            \end{equation}

        where

        .. math::
            \begin{equation}
            q^{\pm} \times (a,a_{age},\Phi) = \frac{1}{1+e^{\pm \Phi \times (x-x_{1/2})}}
            \end{equation}

        .. math::

           \text{Note that: } 0 \leq \Phi \leq 1

        Returns
        -------
        int
            returns the fitness of an animal
        """

        if self.weight == 0:
            return 0
        else:
            q_plus = 1 / (1 + exp(self.phi_age * (self.age - self.a_half)))
            q_minus = 1 / (1 + exp(-1 * self.phi_weight * (self.weight - self.w_half)))
            if 0 < (q_plus * q_minus) <= 1:
                return q_plus * q_minus
            else:
                raise ValueError('Fitness must be in [0, 1].')

    def migration(self, pos, geogr):
        """
        Decide whether or not an animal migrate to a new position

        Parameters
        ----------
        pos : tuple
            position of the animal

        geogr : dict
            The whole map

        Returns
        -------
        pos : tuple
            Returns new position with regards to water
        """
        rand = random.random()
        move_prob = self.fit * self.mu
        old_pos = pos
        if move_prob < rand:
            self.pos = pos
        else:
            direction = ['N', 'E', 'S', 'W']
            move = random.choice(direction)
            if move == 'N':
                self.pos = (pos[0] - 1, pos[1])
            elif move == 'S':
                self.pos = (pos[0] + 1, pos[1])
            elif move == 'E':
                self.pos = (pos[0], pos[1] + 1)
            elif move == 'W':
                self.pos = (pos[0], pos[1] - 1)
        if geogr[self.pos].type == 'water':
            self.pos = old_pos

    def older(self):
        """
        Animal get one year older

        Returns
        -------
        int
            returns new age
        """
        self.age += 1
        self.fit = self.fitness()

    def loss_of_weight(self):
        """
        Animals loses weight

        Returns
        -------
        int
            returns new weight for animal
        """
        self.weight -= (self.weight * self.eta)
        self.fit = self.fitness()

    def death(self):
        r"""
        check if an animal dies

        An animal dies if w = 0 or with a probability:

        .. math::
            \begin{equation}
            \omega \times (1-\Phi)
            \end{equation}

        Returns
        -------
        bool
            True if animal dies
        """
        rand = random.random()
        prob = self.omega * (1 - self.fit)
        if self.weight == 0:
            prob = 1
        if prob > rand:
            return True
        else:
            return False

    def born(self, num):
        r"""
        Check if an animal gives birth

        The probability for giving birth is given by the following equation:

        .. math::
            \begin{equation}
            min(1,\gamma \times \Phi \times (N-1)
            \end{equation}

        N = Number of the same specie in the same position

        The probability of birth is also 0 if the weight is:

        .. math::
            \begin{equation}
            w < \zeta(w_{birth} + \sigma_{birth})
            \end{equation}

        Parameters
        ----------
        num: int
            number of animal in landscape

        Returns
        -------
        bool
            True if an animal gives birth
        """
        rand = random.random()
        probability = min(1, self.gamma * self.fit * (num - 1))

        if self.weight < self.zeta * (self.w_birth + self.sigma_birth):
            probability = 0
        if probability > rand:

            # print('!!!!!!!!!!!!!!!!!!!!!!!!!')
            return True
        else:
            # print('born')
            return False


class Herbivores(Animals):
    """
    Makes a Herbivore
    """
    w_birth = 8.0
    sigma_birth = 1.5
    beta = 0.9
    eta = 0.05
    a_half = 40.0
    phi_age = 0.6
    w_half = 10.0
    phi_weight = 0.1
    mu = 0.25
    gamma = 0.2
    zeta = 3.5
    xi = 1.2
    omega = 0.4
    F = 10.0
    default_params = {'w_birth': w_birth,
                      'sigma_birth': sigma_birth,
                      'beta': beta,
                      'eta': eta,
                      'a_half': a_half,
                      'phi_age': phi_age,
                      'w_half': w_half,
                      'phi_weight': phi_weight,
                      'mu': mu,
                      'gamma': gamma,
                      'zeta': zeta,
                      'xi': xi,
                      'omega': omega,
                      'F': F}



    def feeding(self):
        """
        Change in weight after eating

        Returns
        -------
        float
            returns new weight
        """
        self.weight += self.beta * self.F
        self.fit = self.fitness()


class Carnivores(Animals):
    """
    Makes a Carnivore
    """
    w_birth = 6.0
    sigma_birth = 1.0
    beta = 0.75
    eta = 0.125
    a_half = 40.0
    phi_age = 0.3
    w_half = 4.0
    phi_weight = 0.4
    mu = 0.4
    gamma = 0.8
    zeta = 3.5
    xi = 1.1
    omega = 0.8
    F = 50.0
    DeltaPhiMax = 10.0
    default_params = {'w_birth': w_birth,
                      'sigma_birth': sigma_birth,
                      'beta': beta,
                      'eta': eta,
                      'a_half': a_half,
                      'phi_age': phi_age,
                      'w_half': w_half,
                      'phi_weight': phi_weight,
                      'mu': mu,
                      'gamma': gamma,
                      'zeta': zeta,
                      'xi': xi,
                      'omega': omega,
                      'F': F,
                      'DeltaPhiMax': DeltaPhiMax}


    def kill(self, herbivore):
        r"""
        Decides if Carnivore kills a Herbivore,
        the probability for a Carnivore to kill a Herbivore is given by:

        .. math::
            \begin{equation}
            p =
            \begin{cases}
                0, & \text{if } \Phi_{carn} \leq \Phi_{herb} \\
                \frac{\Phi_{carn}-\Phi_{herb}}{\Delta \Phi_{max}},
                 & \text{if } 0 < \Phi_{carn} - \Phi_{herb} < \Delta \Phi_{max} \\
                1, & \text{otherwise }
            \end{cases}
            \end{equation}

        Parameters
        ----------
        herbivore: object
            the Herbivore that might get eaten

        Returns
        -------
        bool
            Returns True if eaten
        """
        rand = random.random()
        if self.fit <= herbivore.fit:
            p = 0
        elif 0 < self.fit - herbivore.fit < self.DeltaPhiMax:
            p = (self.fit - herbivore.fit) / self.DeltaPhiMax
        else:
            p = 1

        return p > rand

    def feeding(self, weight_herb):
        """
        weight gained after Carnivore eats a Herbivore

        Parameters
        ----------
        weight_herb : int
            the weight of Herbivore

        Returns
        -------
        int
            the new weight for the Carnivore
        """
        self.weight += self.beta * weight_herb
        self.fit = self.fitness()
