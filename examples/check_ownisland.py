# -*- coding: utf-8 -*-

import textwrap
from biosim.simulation import BioSim

"""
Compatibility check for BioSim simulations.

"""
__author__ = 'Aslak Djupskås & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskås@nmbu.no or dinussen.sivarasalingam@nmbu.no'


if __name__ == '__main__':

    geogr = """\
               WWWWWWWWWWWWWWWWWWWWWWW
               WWWWWWWWHWWWWLLLLLLLWWW
               WHHHHHLLLLWWLLLLLLLDWWW
               WHHHHHHHHHWWLLLLLLDDWWW
               WHHHHHLLLLWLLLLLLLWDWWW
               WHHHHHLLLDWLLLHLLLWDWWW
               WHHLLLLLDDDLLLHHHHLDWWW
               WWHHHHLLLDDLLLHWWLLLWWW
               WHHHLLLLLDWLLLLLLLWWWWW
               WHHHHLLLLDWLLLLLLHHWWWW
               WWHHHHLLLLLLLLLWWHHWWWW
               WWWHHHHLLLLLLLLHHHHWWWW
               WWHHHHHHLLWLLLLLLWWWWWW
               WWWWWWWWWWWWWWWWWWWWWWW"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (11, 11),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(90)]}]
    ini_carns = [{'loc': (10, 9),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(40)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                 seed=99004,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 50.0, 'delta': 5},
                             'weight': {'max': 100, 'delta': 5}},
                 vis_years=2, img_years=4)

    sim.set_animal_parameters('Herbivore', {'zeta': 3.2, 'xi': 1.8, 'gamma':0.4})
    sim.set_animal_parameters('Carnivore', {'a_half': 50, 'phi_age': 0.4,
                                            'omega': 0.3, 'F': 60,
                                            'DeltaPhiMax': 12.})
    sim.set_landscape_parameters('L', {'f_max': 900})

    sim.simulate(num_years=20)
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=80)
