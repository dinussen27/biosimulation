"""
Implements a Landscape with different animal species.
"""

import itertools
from biosim.animals import Carnivores, Herbivores
import random

__author__ = 'Aslak Djupskås & Dinussen Sivarassalingam'
__email__ = 'aslak.djupskås@nmbu.no or dinussen.sivarasalingam@nmbu.no'


class Landscape:
    """
    This class contain the common things happening in different landscapes
    """
    f_max = None
    default_params = {'f_max': f_max}

    @classmethod
    def set_params(cls, new_param):
        """
                set Class parameters

                Parameters
                ----------
                new_param: dict
                    given parameters by user

                Raises
                -------
                KeyError, ValueError
                """
        for key in new_param:
            if key not in 'f_max':
                raise KeyError('Invalid parameter name: ' + key)

            if 'f_max' in new_param:
                if not 0 <= new_param['f_max']:
                    raise ValueError('f_max must be positive.')
                cls.f_max = new_param['f_max']

    @classmethod
    def get_params(cls):
        """
        Get class parameters.

        Returns
        -------
        dict
            Dictionary with class parameters.
        """
        return {'f_max': cls.f_max}

    def __init__(self, num_herb=0, num_carn=0):
        """
        Takes number of Herbivores an Carnivores as input and place them at the location
        If nothing is given, the herbivore and carnivore population is zero.

        Parameters
        ----------
        num_herb : int
            initial Herbivores
        num_carn : int
            initial Carnivores
        """

        self.herbivores_pop = [Herbivores() for _ in range(num_herb)]
        self.carnivores_pop = [Carnivores() for _ in range(num_carn)]
        self.type = None

    @property
    def get_num_herb(self):
        """
        Gets number of Herbivores

        Returns
        -------
        int
            Number of Herbivores at the given landscape
        """

        return len(self.herbivores_pop)

    @property
    def get_num_carn(self):
        """
        Gets number of Carnivores

        Returns
        -------
        int
            Number of Carnivores at the given landscape
        """
        return len(self.carnivores_pop)

    @property
    def get_num_animals(self):
        """
        Gets total number of Herbivores and Carnivores

        Returns
        -------
        int
            Number of animals at the given landscape
        """
        return self.get_num_carn + self.get_num_herb

    def feed(self):
        """
            This method feeds all herbivores in one area if enough fodder.
            Then all carnivores hunts in random order on the herbivores, starting at the
            one with lowest fitness

        Returns
        -------
        list
            returns a list of each animal with updated weight if the animal eat.
            The list of herbivores is excluding the herbivores that got eaten.
        """
        food = self.f_max
        self.herbivores_pop.sort(key=lambda atr: atr.fit, reverse=True)
        for herb in self.herbivores_pop:
            if food >= herb.F:
                herb.feeding()
                food -= herb.F

        random.shuffle(self.carnivores_pop)
        for carn in self.carnivores_pop:
            self.herbivores_pop.sort(key=lambda atr: atr.fit, reverse=False)
            eaten = 0
            num_hunt = 0
            while eaten < carn.F and num_hunt < len(self.herbivores_pop):
                target = self.herbivores_pop[num_hunt]
                if carn.kill(target) and target.weight > 0:
                    carn.feeding(target.weight)
                    eaten += target.weight
                    target.weight = 0
                num_hunt += 1
        self.herbivores_pop = [herb for herb in self.herbivores_pop if herb.weight != 0]

    def procreation(self):
        """
        Checks the breeding of all animals at the same position.

        Returns
        -------
        list
            list with the newborns added to the population
        """
        herb_babies = []
        for herb in self.herbivores_pop:
            if herb.born(self.get_num_herb):
                herb_baby = Herbivores(pos=herb.pos)
                if herb.weight > herb_baby.weight:
                    herb_babies.append(herb_baby)
                    herb.weight -= (herb_baby.weight * herb.xi)
        self.herbivores_pop.extend(herb_babies)

        carn_babies = []
        for carn in self.carnivores_pop:
            if carn.born(self.get_num_carn):
                carn_baby = Carnivores(pos=carn.pos)
                if carn.weight > carn_baby.weight:
                    carn_babies.append(carn_baby)
                    carn.weight -= (carn_baby.weight * carn.xi)
        self.carnivores_pop.extend(carn_babies)

    def choose_cell(self, pos, geogr):
        """
        Checks all animals in the same position whether it moves or not.

        Parameters
        ----------
        pos : tuple
            position

        geogr: object
            the map

        Returns
        -------
        list
            list of Herbivores and Carnivores with updated position.
        """

        [herb.migration(pos, geogr) for herb in self.herbivores_pop]
        [carn.migration(pos, geogr) for carn in self.carnivores_pop]
        return self.herbivores_pop, self.carnivores_pop

    def add_migrated_animals(self, all_herbs, all_carns, pos):
        """
        Check all Herbivores and Carnivores (with updates position)
         if they have same position as the given location.

        Parameters
        ----------
        all_herbs : list
            list of all Herbivores
        all_carns : list
            list of all Carnivores
        pos : tuple
            position on the map


        Returns
        -------
        list
            Herbivores and Carnivores that is located in the same position

        """
        self.herbivores_pop = [herb for herb in all_herbs if herb.pos == pos]
        self.carnivores_pop = [carn for carn in all_carns if carn.pos == pos]

    def extendpop(self, num_herbs, num_carns, pos,
                  herb_weight=None, herb_age=0, carn_weight=None, carn_age=0):
        """
        Input from the user, that make it possible to add animals before and during the simulation

        Parameters
        ----------
        num_herbs : int
            Number of Herbivores added
        num_carns : int
            Number of Carnivores added
        pos : tuple
            x and y coordinate of the map
        herb_weight : float
            > 0
        herb_age : int
            ≥ 0
        carn_weight: float
            > 0
        carn_age: int
            ≥ 0
        """

        self.herbivores_pop.extend([Herbivores(pos=pos, weight=herb_weight,
                                               age=herb_age) for _ in range(num_herbs)])
        self.carnivores_pop.extend([Carnivores(pos=pos, weight=carn_weight,
                                               age=carn_age) for _ in range(num_carns)])

    def aging(self):
        """
        Make all the animal in one landscape one year older

        Returns
        -------
        list
            updated ages
        """
        [animal.older() for animal in itertools.chain(self.herbivores_pop, self.carnivores_pop)]

    def weight_lost(self):
        """
        makes all animals at the same location lose weight

        Returns
        -------
        list
            updated weights
        """
        [animal.loss_of_weight() for animal in itertools.chain(
            self.herbivores_pop, self.carnivores_pop)]

    def deaths(self):
        """
        checks all animals in same landscape if each dies.

        Returns
        -------
        list
            returns a list of living animals
        """
        self.herbivores_pop = [herb for herb in self.herbivores_pop if not herb.death()]
        self.carnivores_pop = [carn for carn in self.carnivores_pop if not carn.death()]


class Lowland(Landscape):
    """
    subclass for Landscape with unique specs
    """
    f_max = 800
    default_params = {'f_max': f_max}

    def __init__(self, num_herb=0, num_carn=0):
        """
        Landscape with the most fodder

        Parameters
        ----------
        num_herb : int
            initial Herbivores
        num_carn : int
            initial Carnivores
        """
        super().__init__(num_herb, num_carn)
        self.type = 'lowland'


class Highland(Landscape):
    """
    subclass for Landscape with unique specs
    """
    f_max = 300
    default_params = {'f_max': f_max}

    def __init__(self, num_herb=0, num_carn=0):
        """
         Landscape with less fodder

         Parameters
         ----------
         num_herb : int
             initial Herbivores
         num_carn : int
             initial Carnivores
         """
        super().__init__(num_herb, num_carn)
        self.type = 'highland'


class Desert(Landscape):
    """
    subclass for Landscape with unique specs
    """
    f_max = 0
    default_params = {'f_max': f_max}

    def __init__(self, num_herb=0, num_carn=0):
        """
         Landscape with no fodder

         Parameters
         ----------
         num_herb : int
             initial Herbivores
         num_carn : int
             initial Carnivores
         """
        super().__init__(num_herb, num_carn)
        self.type = 'desert'


class Water(Landscape):
    """
    subclass for Landscape with unique specs
    """
    def __init__(self, num_herb=0, num_carn=0):
        """
         Landscape with no fodder

         Parameters
         ----------
         num_herb : int
             initial Herbivores
         num_carn : int
             initial Carnivores
         """
        super().__init__(num_herb, num_carn)
        self.type = 'water'
