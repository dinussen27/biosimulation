import pytest
import textwrap
from biosim.simulation import BioSim
from biosim.animals import Herbivores, Carnivores
from biosim.landscapes import Lowland, Highland, Desert


class TestBioSim:

    @pytest.fixture(autouse=True)
    def create_biosim(self,):
        """Using this map for each test"""
        self.geogr = """\
                           WWWWWWWWWWWWWWWWWWWWW
                           WWWWWWWWHWWWWLLLLLLLW
                           WHHHHHLLLLWWLLLLLLLWW
                           WHHHHHHHHHWWLLLLLLWWW
                           WHHHHHLLLLLLLLLLLLWWW
                           WHHHHHLLLDDLLLHLLLWWW
                           WHHLLLLLDDDLLLHHHHWWW
                           WWHHHHLLLDDLLLHWWWWWW
                           WHHHLLLLLDDLLLLLLLWWW
                           WHHHHLLLLDDLLLLWWWWWW
                           WWHHHHLLLLLLLLWWWWWWW
                           WWWHHHHLLLLLLLWWWWWWW
                           WWWWWWWWWWWWWWWWWWWWW"""
        self.geogr = textwrap.dedent(self.geogr)
        self.ini_pop = []
        self.BioSim = BioSim(self.geogr, self.ini_pop)

    @pytest.fixture
    def reset_params(self):
        """If changing parameters, this function will reset them afterwords"""
        yield
        Carnivores.set_params(Carnivores.default_params)
        Herbivores.set_params(Herbivores.default_params)
        Lowland.set_params(Lowland.get_params())
        Highland.set_params(Highland.get_params())
        Desert.set_params(Desert.get_params())

    @pytest.mark.parametrize('ini_pop', [[{'loc': (3, 7),
                                           'pop': [{'species': 'Herbivore',
                                                    'age': 10, 'weight': 12.5},
                                                   {'species': 'Herbivore',
                                                    'age': 9, 'weight': 10.3},
                                                   {'species': 'Carnivore',
                                                    'age': 5, 'weight': 8.1}]},
                                          {'loc': (4, 4),
                                           'pop': [{'species': 'Herbivore',
                                                    'age': 10, 'weight': 12.5},
                                                   {'species': 'Carnivore',
                                                    'age': 9, 'weight': 10.3},
                                                   {'species': 'Carnivore',
                                                    'age': 5, 'weight': 8.1}]}],
                                         [{'loc': (3, 9),
                                           'pop': [{'species': 'Herbivore',
                                                    'age': 5, 'weight': 20} for _ in range(150)]}]])
    def test_add_pop(self, ini_pop):
        """
        Testing add_population with two different examples.
        The first example is taken from the task description
        The second is taken from "check_sim"
        """
        self.BioSim.add_population(ini_pop)
        len_pop = 0
        for element in ini_pop:
            len_pop += len(element['pop'])
        assert self.BioSim.island.count_all_animals == len_pop

    def test_set_animal_parameters(self):
        """
        Testing if the parameters for animals change
        """
        self.BioSim.set_animal_parameters('Herbivore', {'F': 300})
        self.BioSim.set_animal_parameters('Carnivore', {'eta': 0.001})
        assert Herbivores.F == 300 and Carnivores.eta == 0.001

    def test_set_landscape_parameters(self):
        """
        Testing if the parameters for landscapes change
        """
        self.BioSim.set_landscape_parameters('L', {'f_max': 7000})
        self.BioSim.set_landscape_parameters('D', {'f_max': 40})
        assert Desert.f_max == 40 and Lowland.f_max == 7000
