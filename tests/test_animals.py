import pytest
from biosim.animals import Herbivores, Carnivores


class TestAnimals:

    @pytest.fixture(autouse=True)
    def create_animal(self):
        """
        Created a object for each animal before the tests
        """
        self.herb = Herbivores()
        self.carn = Carnivores()

    @pytest.fixture
    def reset_animal_defaluts(self):
        """
        If parameters is changed in a test, this function reset the parameters afterwords
        """

        yield
        Carnivores.set_params(Carnivores.default_params)
        Herbivores.set_params(Herbivores.default_params)

    def test_older(self):
        """
        Test if the animals get exactly one year older
        """
        old_age_h = self.herb.age
        old_age_c = self.carn.age
        self.herb.older()
        self.carn.older()
        assert self.herb.age == old_age_h + 1 and self.carn.age == old_age_c + 1

    def test_lose_weight(self):
        """
        Test if the animals lose the weight their suppose to
        """
        assert self.herb.weight > 0
        assert self.carn.weight > 0
        old_weight_herb = self.herb.weight
        old_weight_carn = self.carn.weight
        self.herb.loss_of_weight()
        self.carn.loss_of_weight()
        assert self.herb.weight == pytest.approx(old_weight_herb -
                                                 (old_weight_herb*self.herb.eta))
        assert self.carn.weight == pytest.approx(old_weight_carn -
                                                 (old_weight_carn * self.carn.eta))

    def test_death_not_dead(self, mocker):
        """
        Test if animals do not die if the probability ≈ 0
        """
        mocker.patch("random.random", return_value=1)
        assert self.herb.death() is False and self.carn.death() is False

    def test_death(self, mocker):
        """
        Test if animals do not die if the probability = 1
        """
        mocker.patch("random.random", return_value=0)
        assert self.herb.death() is True and self.carn.death() is True

    def test_born(self, reset_animal_defaluts):
        """
        tests the born-function with fixed parameters

        Parameters
        ----------
        reset_animal_defaluts:
            resets the parameter that changes in the test
        """
        Carnivores.set_params({'gamma': 1000, 'zeta': 0.1})
        Herbivores.set_params({'gamma': 1000, 'zeta': 0.1})
        assert self.carn.born(num=10) is True and self.herb.born(num=10) is True

    def test_fit(self):
        """
        Test if the fitness calculations are correct
        """
        herb = Herbivores(age=40, weight=10)
        assert herb.fit == pytest.approx(0.25)

    def test_fitw0(self):
        """
        If weight is 0, the fitness is 0
        """
        herb = Herbivores(age=2, weight=0)
        assert herb.fit == 0

    def test_kill(self, mocker):
        """
        if p > r return True.
        This tests the opposite
        """
        mocker.patch("random.random", return_value=1)
        assert self.carn.kill(self.herb) is False

    def test_herb_feeding(self):
        """
        Test the correct wight gained after herbivore eat
        """
        old_weight_herb = self.herb.weight
        self.herb.feeding()
        assert self.herb.weight == old_weight_herb + self.herb.beta*self.herb.F

    def test_carn_feeding(self):
        """
        Test the correct wight gained after Carnivore eat
        """
        old_weight_carn = self.carn.weight
        self.carn.feeding(self.herb.weight)
        assert self.carn.weight == old_weight_carn + self.carn.beta*self.herb.weight

    def test_wrong_param(self):
        """
        Test if a ValueError is raised if invalid parameter for 'DeltaPhiMax
        """
        with pytest.raises(ValueError):
            Herbivores()
            self.carn.set_params({'DeltaPhiMax': 0})


class TestError:
    """
    Test some Errors
    """

    def test_age(self):
        """
        Test if a ValueError is raised if invalid age
        """
        with pytest.raises(ValueError):
            Herbivores(age=1.5)

    def test_weight(self):
        """
        Test if a ValueError is raised if invalid age
        """
        with pytest.raises(ValueError):
            Herbivores(weight=-1.5)
