#BioSim Project

This Python package provides package BioSim, which allows
you to simulate the dynamics of the animals on Rossumøya or any other Island. 
The landscape in Rossumøya is divided into four categories;
lowland, which has lots of food, Highland which has less food,
desert which has no food and water which has no food and no 
possibility of entering. 

The two species who lives there are Herbivores, who only eats fodder
from the landscape, and Carnivores, who only eats Herbivores.

The BioSim simulation was inspired from the BioLab simulation, Chutes project, plotting, Randvis project
and our lecture notes which were all made by our Professor Hans Ekkel Plesser.

With this package, one can simulate an island for as many years as one want, and visualize the results along the 
simulation. It is also possible to save the result into a movie.

## Contents

- src: The source code biosim Python package
- examples: Some examples for using the package
- tests: A testsuite
- Documentations


.