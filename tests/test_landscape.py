import pytest
from biosim.landscapes import Landscape, Lowland, Highland, Desert
from biosim.animals import Animals, Herbivores, Carnivores
from scipy.stats import binom_test

# acceptance limit for statistical tests
ALPHA = 0.1


@pytest.mark.parametrize('land', [Lowland, Highland, Desert])
def test_placing_animals(land):
    """
    Function testing that the right amount of aminals are placed in each landscape

    """
    place = land(50, 5)
    assert len(place.herbivores_pop) == 50 and len(place.carnivores_pop) == 5


@pytest.mark.parametrize('num_herbs, num_carns', [(80, 40), (100, 50)])
class TestRecreationDeaths:
    """
    Class testing recreation and deaths for different populations in one landscape
    """

    @pytest.fixture(autouse=True)
    def create_landscape(self, num_herbs, num_carns):
        """
        Creates a landscape with a population for each test in class
        """
        self.num_herbs = num_herbs
        self.num_carns = num_carns
        self.landscape = Landscape(self.num_herbs, self.num_carns)

    @pytest.fixture
    def reset_params(self):
        """
        Resets parameters if changed in test
        """
        yield
        Carnivores.set_params(Carnivores.default_params)
        Herbivores.set_params(Herbivores.default_params)
        Lowland.set_params(Lowland.get_params())
        Highland.set_params(Highland.get_params())
        Desert.set_params(Desert.get_params())

    def test_death(self, mocker):
        """
        Test if death function in landscape works
        """
        herb_old = self.landscape.get_num_herb
        carn_old = self.landscape.get_num_herb
        # mocker.patch("biosim.animals2.random.random", return_value=1)
        mocker.patch("biosim.animals.Herbivores.death", return_value=True)
        mocker.patch("biosim.animals.Carnivores.death", return_value=True)
        for _ in range(100):
            self.landscape.deaths()
            herb = self.landscape.get_num_herb
            carn = self.landscape.get_num_carn
            assert herb <= herb_old
            assert carn <= carn_old
            herb_old = herb
            carn_old = carn

        assert self.landscape.get_num_herb < self.num_herbs
        assert self.landscape.get_num_carn < self.num_carns

    def test_no_death(self):
        """
        Tests the opposite than the previous test
        """
        self.eternal_youth = Desert(self.num_herbs, self.num_carns)
        Herbivores.set_params({'omega': 0})
        Carnivores.set_params({'omega': 0})
        self.eternal_youth.deaths()
        assert self.eternal_youth.get_num_animals == self.num_carns + self.num_herbs

    def test_procreation(self, reset_params):
        """
        Test that the population increases if there are newborns
        """
        Herbivores.set_params({'zeta': 0})
        Carnivores.set_params({'zeta': 0})
        self.landscape.procreation()
        assert self.landscape.get_num_herb > self.num_herbs and\
               self.landscape.get_num_carn > self.num_carns

    def test_all_die(self, reset_params):
        """
        Test that everyone dies when the probability = 1
        """
        Herbivores.set_params({'omega': 750})
        Carnivores.set_params({'omega': 750})
        self.landscape.deaths()
        assert self.landscape.get_num_herb == 0 and self.landscape.get_num_carn == 0

    @pytest.mark.parametrize('phi_age, phi_weight, omega', [(0, 0, (2 / 15)),
                                                            (0, 0, (2 / 3)),
                                                            (0, 0, (6 / 5))])
    def test_death_stat(self, phi_age, phi_weight, omega, reset_params):
        """
        Test death for different probabilities and test that the expected
         value is greater than the significance level

        Parameters
        ----------
        phi_age and phi_weight
            both is zero to make sure the fitness is 1/4
        omega
            different omega gives different probability

        1. p = 0.1
        2. p = 0.5
        3. p = 0.9
        -------
        This function will not always pass
        """
        Herbivores.set_params({'phi_age': phi_age, 'phi_weight': phi_weight, 'omega': omega})
        Carnivores.set_params({'phi_age': phi_age, 'phi_weight': phi_weight, 'omega': omega})
        p_death = omega*(1-0.25)
        # 0.25 is the fitness from the animal.
        # It will allways be 0.25 because phi_age = phi_weight = 0
        ini_herbs = self.landscape.get_num_herb
        ini_carns = self.landscape.get_num_carn
        self.landscape.deaths()
        died_herbs = ini_herbs - self.landscape.get_num_herb
        died_carns = ini_carns - self.landscape.get_num_carn

        pass_herb = binom_test(died_herbs, ini_herbs, p_death) > ALPHA
        pass_carn = binom_test(died_carns, ini_carns, p_death) > ALPHA
        assert pass_herb == pass_carn


class TestFeedingAgingWeightlostAnimals:
    """
    Testing Feeding, Aging ang Weight lost for each animal in a landscape
    """
    @pytest.fixture(autouse=True)
    def create_square(self):
        """creates landscape for each test"""
        self.landscape = Landscape(20, 20)

    def test_carn_hunt(self):
        """
        It is expected that the weight after a carn-hunt is greater than before
        """
        total_weight_start = sum([carn.weight for carn in self.landscape.carnivores_pop])
        Landscape.set_params({'f_max': 0})
        self.landscape.feed()
        total_weight_end = sum([carn.weight for carn in self.landscape.carnivores_pop])
        assert total_weight_start < total_weight_end

    def test_without_carns(self):
        """
        It is expected that the weight after feeding herbovires are greater than before
        """
        total_weight_start = sum([herb.weight
                                  for herb in self.landscape.herbivores_pop])
        Landscape.set_params({'f_max': 100})
        self.landscape.carnivores_pop = []
        self.landscape.feed()
        total_weight_end = sum([herb.weight + (herb.F*herb.beta)
                                for herb in self.landscape.herbivores_pop])
        assert total_weight_start < total_weight_end

    def test_without_herbs(self):
        """
        If no herbivores, no food for canivores and their weight will not change
        """
        total_weight_start = sum([carn.weight for carn in self.landscape.carnivores_pop])
        self.landscape.herbivores_pop = []
        self.landscape.feed()
        total_weight_end = sum([carn.weight for carn in self.landscape.carnivores_pop])
        assert pytest.approx(total_weight_start) == total_weight_end

    def test_animal_ages(self, mocker):
        """
        Test the ages function in Animals by using mocker.spy
        """

        mocker.spy(Animals, 'older')
        num_herb, num_carn = 20, 5
        test = Landscape(num_herb, num_carn)
        test.aging()
        assert Animals.older.call_count == num_herb + num_carn

    def test_weight_lost(self):
        """
        Test that the initial weight is eta times bigger than the final weight
        """
        #self.landscape.carnivores_pop = []
        total_weight_start = sum([carn.weight for carn in self.landscape.carnivores_pop])
        eta = self.landscape.herbivores_pop[0].eta
        self.landscape.f_max = 0
        self.landscape.weight_lost()
        total_weight_end = sum([carn.weight for carn in self.landscape.carnivores_pop])
        assert total_weight_start == total_weight_end


def test_extend_pop():
    """
    Tests that the population gets bigger by using extendpop()
    """
    landscape = Lowland(num_herb=10, num_carn=10)
    landscape.extendpop(num_herbs=10, num_carns=10, pos=())
    assert landscape.get_num_animals == 40


def test_set_f_max():
    """
    Test that the fodder is set
    """
    f_max = 12345
    desert = Desert()
    Desert.set_params({'f_max': f_max})
    assert desert.f_max == f_max


def test_set_f_max_error():
    """
    Test that negative fodder raises ValueError
    """
    with pytest.raises(ValueError):
        f_max = -10
        Desert.set_params({'f_max': f_max})
